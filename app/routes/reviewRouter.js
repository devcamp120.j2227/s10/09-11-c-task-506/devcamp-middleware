// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import review middleware
const reviewMiddleware = require("../middlewares/reviewMiddleware");

router.get("/reviews", reviewMiddleware.getAllReviewMiddleware, (request, response) => {
    response.json({
        message: "Get ALL Review"
    })
})

router.post("/reviews", reviewMiddleware.createReviewMiddleware, (request, response) => {
    response.json({
        message: "Create Review"
    })
})

router.get("/reviews/:reviewId", reviewMiddleware.getDetailReviewMiddleware, (request, response) => {
    const reviewId = request.params.reviewId;

    response.json({
        message: "Get Review ID = " + reviewId
    })
})

router.put("/reviews/:reviewId", reviewMiddleware.updateReviewMiddleware, (request, response) => {
    const reviewId = request.params.reviewId;

    response.json({
        message: "Update Review ID = " + reviewId
    })
})

router.delete("/reviews/:reviewId", reviewMiddleware.deleteReviewMiddleware, (request, response) => {
    const reviewId = request.params.reviewId;

    response.json({
        message: "Delete Review ID = " + reviewId
    })
})

module.exports = router;